package com.example.android.hotelserve.Model;

public class Items {
    private String Name, Image, Price, ItemId;

    public Items() {

    }

    public Items(String name, String image, String price, String itemId) {
        Name = name;
        Image = image;
        Price = price;
        ItemId = itemId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {return Image; }

    public void setImage(String image) {Image = image; }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }
}

package com.example.android.hotelserve;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.android.hotelserve.Interface.ItemClickListener;
import com.example.android.hotelserve.Model.Items;
import com.example.android.hotelserve.ViewHolder.ItemViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ItemList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference itemList;

    String optionId = "";

    FirebaseRecyclerAdapter<Items, ItemViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        database = FirebaseDatabase.getInstance();
        itemList = database.getReference("Item");

        recyclerView = (RecyclerView)findViewById(R.id.recycler_item);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(getIntent() != null)
            optionId = getIntent().getStringExtra("OptionId");
        if(!optionId.isEmpty() && optionId!=null)
        {
            loadListItem(optionId);
        }
    }

    private void loadListItem(String optionId) {
        adapter = new FirebaseRecyclerAdapter<Items, ItemViewHolder>(Items.class,R.layout.item,ItemViewHolder.class,itemList.orderByChild("ItemId").equalTo(optionId)) {
            @Override
            protected void populateViewHolder(ItemViewHolder viewHolder, Items model, int position) {
                viewHolder.ItemName.setText(model.getName());

                final Items local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent itemDetail = new Intent(ItemList.this, ItemDetails.class);
                        itemDetail.putExtra("ItemId", adapter.getRef(position).getKey()); //send item id to new activity
                        startActivity(itemDetail);
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }
}

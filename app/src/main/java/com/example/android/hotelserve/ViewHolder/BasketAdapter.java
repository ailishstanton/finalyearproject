package com.example.android.hotelserve.ViewHolder;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.example.android.hotelserve.Interface.ItemClickListener;
import com.example.android.hotelserve.Model.Order;
import com.example.android.hotelserve.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class BasketViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public TextView txt_basket_name, txt_price;
    public ImageView image_basket_count;

    private ItemClickListener itemClickListener;

    public void setTxt_basket_name(TextView txt_basket_name) {
        this.txt_basket_name = txt_basket_name;
    }

    public BasketViewHolder(@NonNull View itemView) {
        super(itemView);
        txt_basket_name = (TextView)itemView.findViewById(R.id.basket_item_name);
        txt_price = (TextView)itemView.findViewById(R.id.basket_item_price);
        image_basket_count = (ImageView)itemView.findViewById(R.id.basket_item_count);
    }

    @Override
    public void onClick(View v) {

    }
}

public class BasketAdapter extends RecyclerView.Adapter<BasketViewHolder>{

    private List<Order> listData = new ArrayList<>();
    private Context context;

    public BasketAdapter(List<Order> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @Override
    public BasketViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.basket_layout,parent,false);
        return new BasketViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BasketViewHolder holder, int position) {
        TextDrawable drawable = TextDrawable.builder().buildRound(""+listData.get(position).getQuantity(), Color.RED);
        holder.image_basket_count.setImageDrawable(drawable);

        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        int Price = (Integer.parseInt(listData.get(position).getPrice()))*(Integer.parseInt(listData.get(position).getQuantity()));
        holder.txt_price.setText(fmt.format(Price));
        holder.txt_basket_name.setText(listData.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }
}

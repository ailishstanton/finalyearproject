package com.example.android.hotelserve.Model;

public class Room {
    private String Password;

    public Room() {

    }

    public Room(String password) {
        Password = password;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }
}

package com.example.android.hotelserve;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.android.hotelserve.Interface.ItemClickListener;
import com.example.android.hotelserve.Model.Options;
import com.example.android.hotelserve.ViewHolder.OptionsViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class OptionsList extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference optionsList;

    String categoryId= "";

    FirebaseRecyclerAdapter<Options, OptionsViewHolder> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options_list);

        database = FirebaseDatabase.getInstance();
        optionsList = database.getReference("Option");

        recyclerView = (RecyclerView)findViewById(R.id.recycler_options);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if(getIntent() != null)
            categoryId = getIntent().getStringExtra("CategoryId");
        if(!categoryId.isEmpty() && categoryId != null)
        {
            loadListOptions(categoryId);
        }
    }

    private void loadListOptions(String categoryId) {
        adapter = new FirebaseRecyclerAdapter<Options, OptionsViewHolder>(Options.class,R.layout.option_item,OptionsViewHolder.class,optionsList.orderByChild("MenuId").equalTo(categoryId)) {
            @Override
            protected void populateViewHolder(OptionsViewHolder viewHolder, Options model, int position) {
                viewHolder.option_name.setText(model.getName());

                final Options clickItem = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent itemList = new Intent(OptionsList.this,ItemList.class);
                        itemList.putExtra("OptionId", adapter.getRef(position).getKey());
                        startActivity(itemList);
                    }
                });

            }
        };
        recyclerView.setAdapter(adapter);
    }
}

package com.example.android.hotelserve;

import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.android.hotelserve.Database.Database;
import com.example.android.hotelserve.Model.Items;
import com.example.android.hotelserve.Model.Order;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ItemDetails extends AppCompatActivity {

    TextView item_name, item_price;
    ImageView item_image;

    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnBasket;
    ElegantNumberButton numberButton;

    String itemId = "";

    FirebaseDatabase database;
    DatabaseReference Item;

    Items currentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        database = FirebaseDatabase.getInstance();
        Item = database.getReference("Item");

        numberButton = (ElegantNumberButton)findViewById(R.id.numberBtn);
        btnBasket = (FloatingActionButton)findViewById(R.id.btnBasket);

        btnBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToBasket(new Order(
                        itemId,
                        currentItem.getName(),
                        numberButton.getNumber(),
                        currentItem.getPrice()
                ));

                Toast.makeText(ItemDetails.this, "Added to Basket", Toast.LENGTH_SHORT).show();
            }
        });

        item_name = (TextView)findViewById(R.id.item_name);
        item_price = (TextView)findViewById(R.id.item_price);
        item_image = (ImageView)findViewById(R.id.item_image);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //get item id from intent
        if(getIntent() != null)
            itemId = getIntent().getStringExtra("ItemId");
        if(!itemId.isEmpty()) {
            getDetailItem(itemId);
        }
    }

    private void getDetailItem(String itemId) {
        Item.child(itemId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentItem = dataSnapshot.getValue(Items.class);

                //get image
                Picasso.with(getBaseContext()).load(currentItem.getImage()).into(item_image);
                collapsingToolbarLayout.setTitle(currentItem.getName());

                item_price.setText(currentItem.getPrice());

                item_name.setText(currentItem.getName());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

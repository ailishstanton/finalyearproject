package com.example.android.hotelserve;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.hotelserve.Common.Common;
import com.example.android.hotelserve.Database.Database;
import com.example.android.hotelserve.Model.Order;
import com.example.android.hotelserve.Model.Request;
import com.example.android.hotelserve.ViewHolder.BasketAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Basket extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference requests;

    TextView txtTotalPrice;
    Button btnPlace;

    List<Order> basket = new ArrayList<>();
    BasketAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);

        database = FirebaseDatabase.getInstance();
        requests = database.getReference("Requests");

        //init
        recyclerView = (RecyclerView)findViewById(R.id.listBasket);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        txtTotalPrice = (TextView)findViewById(R.id.total);
        btnPlace = (Button)findViewById(R.id.btnPlaceOrder);

        btnPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Request request = new Request(
                        Common.getCurrent_room(),
                        txtTotalPrice.getText().toString(),
                        basket
                );
                //submit to Firebase
                requests.child(String.valueOf(System.currentTimeMillis())).setValue(request);
                //Delete basket
                new Database(getBaseContext()).clearBasket();
                Toast.makeText(Basket.this, "Thank you, your oder has been placed", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
        
        loadListItems();
    }

    private void loadListItems() {
        basket = new Database(this).getBasket();
        adapter = new BasketAdapter(basket,this);
        recyclerView.setAdapter(adapter);

        //calculate price
        int total = 0;
        for(Order order:basket)
            total+=(Integer.parseInt(order.getPrice()))*(Integer.parseInt(order.getQuantity()));
        Locale locale = new Locale("en", "UK");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);

        txtTotalPrice.setText(fmt.format(total));
    }
}

package com.example.android.hotelserve.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.android.hotelserve.Interface.ItemClickListener;
import com.example.android.hotelserve.R;

public class OptionsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView option_name;

    private ItemClickListener itemClickListener;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public OptionsViewHolder(View itemView){
        super(itemView);

        option_name = (TextView)itemView.findViewById(R.id.option_name);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        itemClickListener.onClick(view, getAdapterPosition(), false);

    }
}

package com.example.android.hotelserve.Model;

public class Options {
    private String Name, MenuId;

    public Options() {

    }

    public Options(String name, String menuId) {
        Name = name;
        MenuId = menuId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }
}

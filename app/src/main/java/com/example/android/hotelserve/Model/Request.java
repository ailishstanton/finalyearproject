package com.example.android.hotelserve.Model;

import com.example.android.hotelserve.Common.Common;

import java.util.List;

public class Request {
    private String total;
    private List<Order> items;

    public Request() {
    }

    public Request(Room room, String total, List<Order> items) {
        Common.current_room = room;
        this.total = total;
        this.items = items;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<Order> getItems() {
        return items;
    }

    public void setItems(List<Order> items) {
        this.items = items;
    }
}

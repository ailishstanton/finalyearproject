package com.example.android.hotelserve;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.android.hotelserve.Common.Common;
import com.example.android.hotelserve.Model.Room;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    Button SignIn;
    EditText roomNumber, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SignIn = (Button)findViewById(R.id.signIn);

        roomNumber = (EditText)findViewById(R.id.roomNumber);
        password = (EditText)findViewById(R.id.password);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_Room = database.getReference("Room");

        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                table_Room.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        //check room number
                        if(dataSnapshot.child(roomNumber.getText().toString()).exists()) {
                            //check password
                            Room room = dataSnapshot.child(roomNumber.getText().toString()).getValue(Room.class);
                            if (room.getPassword().equals(password.getText().toString())) {
                                Intent homeIntent = new Intent(MainActivity.this,Home.class);
                                Common.current_room = room;
                                startActivity(homeIntent);
                                finish();
                            } else
                                Toast.makeText(MainActivity.this, "Incorrect details entered", Toast.LENGTH_SHORT).show();
                        }
                        else
                            Toast.makeText(MainActivity.this, "Invalid room number entered", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        });

    }
}
